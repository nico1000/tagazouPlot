# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 12:02:31 2019

@author: mille
"""

from PyQt5 import QtWidgets, uic
import matplotlib.pyplot as plt
import os

ui_form_param, ui_base_param = uic.loadUiType(
    os.path.dirname(os.path.realpath(__file__)) + r'\tagazouPlot_ParamDialog.ui')


class paramWidget(ui_base_param, ui_form_param):

    def __init__(self, fig, parent=None):
        super(ui_base_param, self).__init__(parent)
        self.setupUi(self)
        self.figu = fig

        self.connectActions()

    def connectActions(self):
        self.pushButton_cancel.clicked.connect(self.cancel)

        self.pushButton_applyGrid.clicked.connect(self.applyGrid)
        self.pushButton_applyAxis.clicked.connect(self.applySpines)
        self.pushButton_applyTicks.clicked.connect(self.applyTicks)
        self.pushButton_apply.clicked.connect(self.apply)

        self.pushButton_axesColor.clicked.connect(self.chooseColorAxes)
        self.pushButton_majGridCol.clicked.connect(self.chooseColorMajGrid)
        self.pushButton_minGridCol.clicked.connect(self.chooseColorMinGrid)
        self.pushButton_ticksColor.clicked.connect(self.chooseColorTicks)
        self.lineEdit_col.textChanged.connect(self.setButtonAxesColor)
        self.lineEdit_majGridCol.textChanged.connect(self.setButtonMajGridColor)
        self.lineEdit_minGridCol.textChanged.connect(self.setButtonMinGridColor)
        self.lineEdit_tickCol.textChanged.connect(self.setButtonTicksColor)

        self.pushButton_saveParam.clicked.connect(self.saveParamsList)
        self.pushButton_loadParam.clicked.connect(self.setParamsFromFile)

    def printi(self):
        # Function de test
        print('iiiiiiiii')

    def cancel(self):
        self.close()
        print('TOTAL END')

    def apply(self):
        """
        Function that apply all parameters contains in the param widget
        which means spines, ticks, grid
        and draw to update figure
        (it's a bit a "doublon" because it is done also in each applyStuff function)
        """
        #           Spines
        self.applySpines()
        #           Ticks
        self.applyTicks()
        #           Grid
        self.applyGrid()
        #           draw to update
        self.figu.canvas.draw()

    def applySpines(self):
        """
        Function to update the spine parameter from the Axis tab
        """
        self.ax = self.figu.axes[0]
        # Get the color of spines wanted
        color = self.lineEdit_col.text()
        if color == '':
            color = 'k'
        # Get the size of the spines
        width = self.doubleSpinBox_wid.value()
        # Set the spine visible or not depending on the choice wanted.
        # combobox_sp (sp for spine) has index 0=hidden and 1=visible
        self.ax.spines['bottom'].set_visible(self.comboBox_sp_down.currentIndex())
        self.ax.spines['left'].set_visible(self.comboBox_sp_left.currentIndex())
        self.ax.spines['top'].set_visible(self.comboBox_sp_up.currentIndex())
        self.ax.spines['right'].set_visible(self.comboBox_sp_right.currentIndex())
        # Set color and size for all visible spines
        [i.set_linewidth(width) for i in self.ax.spines.values()]
        [i.set_color(color) for i in self.ax.spines.values()]
        # Draw to update fig
        self.figu.canvas.draw()

    def applyTicks(self):
        """
        Function to apply the tick modification from the tick tab
        """
        self.ax = self.figu.axes[0]
        # Set the number of ticks
        self.ax.locator_params(axis='x', nbins=self.spinBox_xticks.value())
        self.ax.locator_params(axis='y', nbins=self.spinBox_yticks.value())
        # Get the width and length of ticks
        majwidth = self.doubleSpinBox_majWid.value()
        majlen = self.doubleSpinBox_majLen.value()
        minwidth = self.doubleSpinBox_minWid.value()
        minlen = self.doubleSpinBox_minLen.value()
        # Get the choice index of major x ticks (None, Bottom, etc...)
        xticks = self.comboBox_major_x.currentIndex()
        yticks = self.comboBox_major_y.currentIndex()
        # From the index of major x tick choice, set to true or false corresponding
        # tick position wanted to be visible
        if xticks == 0:
            bottom = False
            top = False
        elif xticks == 1:
            bottom = True
            top = False
        elif xticks == 2:
            bottom = False
            top = True
        elif xticks == 3:
            bottom = True
            top = True
        # Apply the tick modification
        self.ax.tick_params(axis='x', which='major', bottom=bottom, top=top,
                            width=majwidth, length=majlen,
                            direction=self.comboBox_majorDir_x.currentText(),
                            labelsize=self.comboBox_tickLabelSize.currentText(),
                            pad=self.doubleSpinBox_tickLabelPad_x.value(),
                            color=self.lineEdit_tickCol.text())
        # The same as previously for y ticks
        if yticks == 0:
            left = False
            right = False
        elif yticks == 1:
            left = True
            right = False
        elif yticks == 2:
            left = False
            right = True
        elif yticks == 3:
            left = True
            right = True
        self.ax.tick_params(axis='y', which='major', left=left, right=right,
                            width=majwidth, length=majlen,
                            direction=self.comboBox_majorDir_y.currentText(),
                            labelsize=self.comboBox_tickLabelSize.currentText(),
                            pad=self.doubleSpinBox_tickLabelPad_y.value(),
                            color=self.lineEdit_tickCol.text())

        # The same as for the major x and y ticks, now for the minor ticks
        # no need anymore for labelsize and pad
        xticks = self.comboBox_minor_x.currentIndex()
        yticks = self.comboBox_minor_y.currentIndex()

        if xticks == 0 and yticks == 0:
            plt.minorticks_off()
        else:
            plt.minorticks_on()
            if xticks == 0:
                bottom = False
                top = False
            elif xticks == 1:
                bottom = True
                top = False
            elif xticks == 2:
                bottom = False
                top = True
            elif xticks == 3:
                bottom = True
                top = True
            self.ax.tick_params(axis='x', which='minor', bottom=bottom, top=top,
                                width=minwidth, length=minlen,
                                direction=self.comboBox_minorDir_x.currentText(),
                                color=self.lineEdit_tickCol.text())
            if yticks == 0:
                left = False
                right = False
            elif yticks == 1:
                left = True
                right = False
            elif yticks == 2:
                left = False
                right = True
            elif yticks == 3:
                left = True
                right = True
            self.ax.tick_params(axis='y', which='minor', left=left, right=right,
                                width=minwidth, length=minlen,
                                direction=self.comboBox_minorDir_y.currentText(),
                                color=self.lineEdit_tickCol.text())
        # Draw to update figure
        self.figu.canvas.draw()

    def applyGrid(self):
        """
        Function that apply the grid change from the grid tab widget
        """
        self.ax = self.figu.axes[0]
        # Set major grid
        if self.comboBox_majGrid.currentIndex() == 0:
            self.ax.grid(b=False, which='major', axis='both')
        else:
            self.ax.grid(b=True, which='major', axis='both',
                         linestyle=self.comboBox_majGridStyle.currentText(),
                         alpha=self.doubleSpinBox_majGridAlpha.value(),
                         color=self.lineEdit_majGridCol.text(),
                         linewidth=self.doubleSpinBox_majGridSize.value())
        # set minor grid
        if self.comboBox_minGrid.currentIndex() == 0:
            self.ax.grid(b=False, which='minor', axis='both')
        else:
            self.ax.grid(b=True, which='minor', axis='both',
                         linestyle=self.comboBox_minGridStyle.currentText(),
                         alpha=self.doubleSpinBox_minGridAlpha.value(),
                         color=self.lineEdit_minGridCol.text(),
                         linewidth=self.doubleSpinBox_minGridSize.value())
        # Draw to update figure
        self.figu.canvas.draw()

    # =============================================================================
    # Function to handle the color
    # When the color is changed, change the corresponding button color
    # =============================================================================

    def chooseColorAxes(self):
        col = QtWidgets.QColorDialog().getColor()
        self.lineEdit_col.setText(col.name())

    def setButtonAxesColor(self):
        s = 'font: 11pt "MS Shell Dlg 2"; \n'
        colname = self.lineEdit_col.text()
        self.pushButton_axesColor.setStyleSheet(s + 'background-color: ' + colname)

    def chooseColorMajGrid(self):
        col = QtWidgets.QColorDialog().getColor()
        self.lineEdit_majGridCol.setText(col.name())

    def setButtonMajGridColor(self):
        s = 'font: 11pt "MS Shell Dlg 2"; \n'
        colname = self.lineEdit_majGridCol.text()
        self.pushButton_majGridCol.setStyleSheet(s + 'background-color: ' + colname)

    def chooseColorMinGrid(self):
        col = QtWidgets.QColorDialog().getColor()
        self.lineEdit_minGridCol.setText(col.name())

    def setButtonMinGridColor(self):
        s = 'font: 11pt "MS Shell Dlg 2"; \n'
        colname = self.lineEdit_minGridCol.text()
        self.pushButton_minGridCol.setStyleSheet(s + 'background-color: ' + colname)

    def chooseColorTicks(self):
        col = QtWidgets.QColorDialog().getColor()
        self.lineEdit_tickCol.setText(col.name())

    def setButtonTicksColor(self):
        s = 'font: 11pt "MS Shell Dlg 2"; \n'
        colname = self.lineEdit_tickCol.text()
        self.pushButton_ticksColor.setStyleSheet(s + 'background-color: ' + colname)

    # =============================================================================
    # Fonctions pour la sauvegarde des parametres dans un fichier texte
    # et la réutilisation des parametres à partir du fichier texte
    # =============================================================================

    def get_widgetValue(self, w):
        """
        Get widget value as a string
        Use for the save and load plot params
        """
        if type(w) == type(QtWidgets.QSpinBox()) or type(w) == type(QtWidgets.QDoubleSpinBox()):
            return str(w.value())
        if type(w) == type(QtWidgets.QLineEdit()):
            return w.text()
        if type(w) == type(QtWidgets.QComboBox()):
            return str(w.currentIndex())

    def set_widgetValue(self, w, val):
        """
        Set widget value from a string
        Use for the save and load plot params
        """
        if type(w) == type(QtWidgets.QSpinBox()):
            w.setValue(float(val))
            return w.value()
        if type(w) == type(QtWidgets.QDoubleSpinBox()):
            w.setValue(float(val))
            return w.value()
        if type(w) == type(QtWidgets.QLineEdit()):
            w.setText(val)
            return w.text()
        if type(w) == type(QtWidgets.QComboBox()):
            w.setCurrentIndex(int(val))
            return w.currentIndex()

    def createSaveList(self):
        """
        Function that create a list of string in the form:
        widgetName \t widgetvalue \n
        each separated by the corresponding tab (axis, ticks, grid)

        /!\ If you modify the list format, especially the "spin param\n" etc.
        You must modify also the setParamFromList function

        Return the list
        """
        spine = self.tab_spine.children()
        spineParams = [i.objectName() + '\t' + self.get_widgetValue(i) + '\n' for i in spine if
                       type(i) == type(QtWidgets.QSpinBox()) or type(i) == type(QtWidgets.QLineEdit()) or type(
                           i) == type(QtWidgets.QComboBox()) or type(i) == type(QtWidgets.QDoubleSpinBox())]

        ticks = self.tab_ticks.children()
        ticksParams = [i.objectName() + '\t' + self.get_widgetValue(i) + '\n' for i in ticks if
                       type(i) == type(QtWidgets.QSpinBox()) or type(i) == type(QtWidgets.QLineEdit()) or type(
                           i) == type(QtWidgets.QComboBox()) or type(i) == type(QtWidgets.QDoubleSpinBox())]

        grid = self.tab_grid.children()
        gridParams = [i.objectName() + '\t' + self.get_widgetValue(i) + '\n' for i in grid if
                      type(i) == type(QtWidgets.QSpinBox()) or type(i) == type(QtWidgets.QLineEdit()) or type(
                          i) == type(QtWidgets.QComboBox()) or type(i) == type(QtWidgets.QDoubleSpinBox())]

        plotParams = ['# Plot param file. Do not modify manually! Load it, modify it and resave it\n'] + [
            'spine param\n'] + spineParams + ['ticks param\n'] + ticksParams + ['grid param\n'] + gridParams + [
                         'End Param\n']

        return plotParams

    def setParamsFromList(self, paramsList):
        """
        Function that set the parameters of all tabs (axis, ticks, grid)
        from a list of parameters created by the function createSaveList
        or created from a readfile of a file where a createSaveList has been written

        input: paramsList: a list in the exact form of a createSaveList
        return: rien
        """
        print('start setting')
        # Get the widgets from the tabs and create a complete widgets list
        spineWidgets = self.tab_spine.children()
        ticksWidgets = self.tab_ticks.children()
        gridWidgets = self.tab_grid.children()
        widgets = spineWidgets + ticksWidgets + gridWidgets

        # Get the index where to start and end the differents widget params
        # element from the paramsList
        spineIndex1 = paramsList.index('spine param\n') + 1
        spineIndex2 = paramsList.index('ticks param\n')
        ticksIndex1 = paramsList.index('ticks param\n') + 1
        ticksIndex2 = paramsList.index('grid param\n')
        gridIndex1 = paramsList.index('grid param\n') + 1
        gridIndex2 = paramsList.index('End Param\n')

        # Create the list of widgets params to be pass to the
        # widgets of the widgets list
        # in the form: [widgetName \t value \n]
        spineParams = paramsList[spineIndex1:spineIndex2]
        ticksParams = paramsList[ticksIndex1:ticksIndex2]
        gridParams = paramsList[gridIndex1:gridIndex2]
        params = spineParams + ticksParams + gridParams

        # Set the widgets values from the widget params list created just above
        # if there is a correspondance between the widgetName of the params list
        # and the name of the widget from the widgets list
        for wid in widgets:
            for par in params:
                papaName, papaVal = par.split()
                if papaName == wid.objectName():
                    self.set_widgetValue(wid, papaVal)
        print('load setting done')

    def setParamsFromFile(self):
        ftype = "Text files (*.txt)"
        # Ask for a file name, which is return in the first element of the list 'rep'
        rep = QtWidgets.QFileDialog.getOpenFileName(self, "File to plot", filter=ftype)
        if rep[0] != '':
            file = open(rep[0], 'r')
            paramsList = file.readlines()
            file.close()
            print('setup read')
            self.setParamsFromList(paramsList)

    def saveParamsList(self):
        ftype = "Text files (*.txt)"
        # Ask for a file name, which is return in the first element of the list 'rep'
        rep = QtWidgets.QFileDialog.getSaveFileName(self, "File to save plot params", filter=ftype)
        if rep[0] != '':
            try:
                file = open(rep[0], 'x')
                print('File new')
            except FileExistsError:
                file = open(rep[0], 'w')
                print('Erase existing file')

            paramsList = self.createSaveList()
            file.writelines(paramsList)
            file.close()


if __name__ == '__main__':
    import numpy as np

    # plt.style.use(r'D:\NicoMille\THESE\VRAC_Python\Python_tagazouPLot\mpl_simple1.mplstyle')
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(np.arange(1, 11, 1))
    paramdialog = paramWidget(fig)
    paramdialog.show()
