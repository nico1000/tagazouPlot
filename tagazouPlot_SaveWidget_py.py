# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 17:33:24 2019

@author: mille
"""

from PyQt5 import QtWidgets, uic
from matplotlib import pyplot as plt
import time, os

ui_form_save, ui_base_save = uic.loadUiType(os.path.dirname(os.path.realpath(__file__)) + r'\tagazouPlot_SaveWidget.ui')


class saveWidget(ui_base_save, ui_form_save):

    savepath = ''

    def __init__(self, parent=None):
        super(ui_base_save, self).__init__(parent)
        self.setupUi(self)
        self.connectActions()
        self.updateFig()

    def connectActions(self):
        self.pushButton_cancel.clicked.connect(self.cancel)
        self.pushButton_path.clicked.connect(self.selectFile)
        self.pushButton_save.clicked.connect(self.save)

    def selectFile(self):
        """
        Ask for a file path (existing or not)
        and save it in the self.savepath class variable
        Function called by the pushbutton_path
        """
        ftype = "Text files (*.txt)"
        # Ask for a file name, which is return in the first element of the list 'rep'
        rep = QtWidgets.QFileDialog.getSaveFileName(self, "File to plot", filter=ftype)
        if rep[0] != '':
            self.savepath = rep[0]
            self.label_path.setText('Path: ' + self.savepath)

    def cancel(self):
        print('Not Saved')
        self.close()

    def updateFig(self):
        """
        Update the fig combobox
        that is used to select which figure to save
        """
        self.figs = plt.get_fignums()
        self.comboBox_fig.clear()
        for fignum in self.figs:
            self.comboBox_fig.addItem('Figure '+str(fignum))

    def save(self):
        """
        Function called by the save button
        Call the createSaveList function to get
        the list of string to be saved
        and write it in the self.savepath file
        return True if the file is well saved
        return False otherwise
        """

        what = self.comboBox_saveChoice.currentIndex()

        if self.comboBox_fig.currentText() == '':
            print('No figure')
            return False
        else:
            # Set the figure to be saved with the combobox fig
            figNum = int(self.comboBox_fig.currentText().split()[-1])
            figu = plt.figure(figNum)

        if self.savepath != '':
            # Call the createSaveList function to get
            # the list of string to be saved
            savelist = self.createSaveList(what, figu)
            # Check if the file exist or not
            # and open it with the right open parameter
            # x = open new file
            # w = open existing file and erase what is inside
            try:
                file = open(self.savepath,'x')
                print('File new')
            except FileExistsError:
                file = open(self.savepath,'w')
                print('Erase existing file')
            # Write the savelist into the file
            file.writelines(savelist)
            file.close()

            print('saved')
            return True
        else:
            self.label_path.setText('Choose a path !')
            print('Choose a path')
            return False

    def createSaveList(self, what, figu):
        """
        Function that create the savelist
        list of string that will be saved
        depending on the what param: the choose combobox index
        return:     savelist: the mega list of string to be written
        """
        savelist = []
        savelist += ['# ' + self.lineEdit_header.text() + '\n']
        savelist += ['# save date and time: ' + time.asctime() + '\n']

        if what == 0:
            # What = 0 correspond to only data
            datalist = figu.createSaveDataList()
            savelist += datalist

        elif what == 1:
            # What = 1 correspond to data and line style
            datalist = figu.createDataAndStyleList()
            savelist += datalist

        elif what == 2:
            # What = 2 correspond to data, line style
            # and the paramdialog widget (=template)
            paramList = figu.paramdialog.createSaveList()
            savelist += paramList
            datalist = figu.createDataAndStyleList()
            savelist += datalist

        return savelist


if __name__ == '__main__':
    from tagazouPlot_myFigure import myFigure

    fig1 = plt.figure(FigureClass=myFigure)
    fig1.customizeToolbar()
    ax = fig1.add_subplot(111)
    ax.plot([1,2,3],[3,4,6])


